export const fonts = {
  'nunito-regular': require('../assets/fonts/Nunito-Regular.ttf'),
  'nunito-bold': require('../assets/fonts/Nunito-Bold.ttf'),
}