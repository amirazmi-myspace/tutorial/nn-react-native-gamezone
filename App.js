import React from 'react'
import { AppLoading } from 'expo'
import { useFonts } from 'expo-font'
import { fonts } from './utils/fonts'
import { AppNavigator } from './routes/drawer'

export default function App() {
  let [fontsLoaded] = useFonts(fonts)

  if (!fontsLoaded) return <AppLoading />
  if (fontsLoaded) return <AppNavigator />
}

