import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'
import { AboutStack } from './AboutStack'
import { HomeStack } from './HomeStack'

const { Screen, Navigator } = createDrawerNavigator()

const RootDrawerNavigator = () => (
  <Navigator initialRouteName='Home'>
    <Screen component={HomeStack} name='Home' />
    <Screen component={AboutStack} name='About' />
  </Navigator>
)

export const AppNavigator = () => <NavigationContainer><RootDrawerNavigator /></NavigationContainer>
