import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ReviewDetails from '../screens/ReviewDetails'
import Home from '../screens/Home'
import Header from '../shared/Header'
import { Image } from 'react-native'

const { Navigator, Screen } = createStackNavigator()

export const HomeStack = () => (
  <Navigator initialRouteName='Home' screenOptions={screenOptions}>
    <Screen name='Home' component={Home} options={options.home} />
    <Screen name='ReviewDetails' component={ReviewDetails} options={{ title: 'Review Details' }} />
  </Navigator >
)

const options = {
  home: ({ navigation }) => {
    return {
      headerTitle: () => <Header title='GameZone' navigation={navigation} />,
      headerBackground: () => <Image source={require('../assets/game_bg.png')} style={{ height: 80 }} />
    }
  }
}

const screenOptions = {
  headerTintColor: '#444',
  headerStyle: { backgroundColor: '#eee', height: 80 },
  headerTitleStyle: { textTransform: 'uppercase' }
}