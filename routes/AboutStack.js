import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import About from '../screens/About'
import Header from '../shared/Header'
import { Image } from 'react-native'

const { Navigator, Screen } = createStackNavigator()

export const AboutStack = () => (
  <Navigator initialRouteName='About' screenOptions={screenOptions}>
    <Screen name='About' component={About} options={options.about} />
  </Navigator >
)

const options = {
  about: ({ navigation }) => {
    return {
      headerTitle: () => <Header title='About' navigation={navigation} />,
      headerBackground: () => <Image source={require('../assets/game_bg.png')} style={{ height: 80 }} />
    }
  }
}

const screenOptions = {
  headerTintColor: '#444',
  headerStyle: { backgroundColor: '#eee', height: 80 },
  headerTitleStyle: { textTransform: 'uppercase' }
}