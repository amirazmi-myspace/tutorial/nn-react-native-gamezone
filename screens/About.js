import React from 'react'
import { Text, View } from 'react-native'
import { globalStyles } from '../utils/globalStyles'

export default function About() {
  return (
    <View style={globalStyles.container}>
      <Text>About Screem</Text>
    </View>
  )
}