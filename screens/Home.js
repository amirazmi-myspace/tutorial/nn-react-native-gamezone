import React, { useState } from 'react'
import { Keyboard, Modal, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import { MaterialIcons } from '@expo/vector-icons'
import Card from '../shared/Card'
import { globalStyles } from '../utils/globalStyles'
import ReviewForm from './ReviewForm'

export default function Home({ navigation }) {

  const [modalOpen, setModalOpen] = useState(false)
  const [reviews, setReviews] = useState([
    { title: 'Zelda, Breath of Fresh Air', rating: 5, body: 'lorem ipsum', key: '1' },
    { title: 'Gotta Catch Them All (Again)', rating: 4, body: 'lorem ipsum', key: '2' },
    { title: 'Not So "Finale"', rating: 3, body: 'lorem ipsum', key: '3' },
  ])

  const addReview = (review) => {
    review.key = Math.random()
    setReviews((currentReviews) => [...currentReviews, review])
    setModalOpen(false)
  }

  const renderReviews = ({ item }) => (
    <TouchableOpacity onPress={() => navigation.navigate('ReviewDetails', item)}>
      <Card>
        <Text style={globalStyles.titleText}>{item.title}</Text>
      </Card>
    </TouchableOpacity>
  )

  const renderModal = () => (
    <Modal visible={modalOpen} animationType='slide'>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.modalContent}>
          <MaterialIcons name='close' size={24} style={{ ...styles.modalToggle, ...styles.modalClose }} onPress={() => setModalOpen(false)} />
          <ReviewForm addReview={addReview} />
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  )

  return (
    <View style={globalStyles.container}>
      {renderModal()}
      <MaterialIcons name='add' size={24} style={styles.modalToggle} onPress={() => setModalOpen(true)} />
      <FlatList data={reviews} renderItem={renderReviews} />
    </View>
  )
}
const styles = StyleSheet.create({
  modalContent: { flex: 1 },
  modalToggle: {
    marginBottom: 10,
    borderWidth: 1,
    borderColor: '#f2f2f2',
    padding: 10,
    borderRadius: 10,
    alignSelf: 'center'
  },
  modalClose: {
    marginTop: 20,
    marginBottom: 0
  }
})